import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    version: null,
    isLandscape: false,
    animations: [
      'none',
      'animated fadeIn',
      'animated bounce',
      'animated shake',
      'animated zoomInRight',
      'animated bounceInUp',
      'animated pulse',
      'animated tada',
      'animated flipInY',
      'animated swing',
      'animated wobble',
      'animated fadeInUp',
      'animated lightSpeedIn',
      'animated fadeInRight',
      'animated fadeInLeft'
    ],
    backgrounds: [
      require(`@/assets/background/appa-meeting.jpg`),
      require(`@/assets/background/appeasement.jpg`),
      require(`@/assets/background/better-deal.jpg`),
      require(`@/assets/background/continuing-sanctions.jpg`),
      require(`@/assets/background/deal-tiger-directly.jpg`),
      require(`@/assets/background/defensive-pact.jpg`),
      require(`@/assets/background/diplomacy.jpg`),
      require(`@/assets/background/funeral.jpg`),
      require(`@/assets/background/give-tigers.jpg`),
      require(`@/assets/background/going-to-war.jpg`),
      require(`@/assets/background/guerilla-tactics.jpg`),
      require(`@/assets/background/high-rock.jpg`),
      require(`@/assets/background/impose-sanctions.jpg`),
      require(`@/assets/background/intro-interior.jpg`),
      require(`@/assets/background/intro.jpg`),
      require(`@/assets/background/lifting-sanctions.jpg`),
      require(`@/assets/background/lion-alliance.jpg`),
      require(`@/assets/background/lion-throne.jpg`),
      require(`@/assets/background/play.jpg`),
      require(`@/assets/background/war.jpg`),
      require(`@/assets/background/water-celebration.jpg`)
    ],
    characters: [
      [
        require(`@/assets/images/prince-connor/connor-optimistic.png`),
        require(`@/assets/images/prince-connor/connor-optimistic.png`),
        require(`@/assets/images/prince-connor/connor-angry.png`),
        require(`@/assets/images/prince-connor/connor-displease.png`),
        require(`@/assets/images/prince-connor/connor-dilemma.png`),
        require(`@/assets/images/prince-connor/connor-implore.png`),
        require(`@/assets/images/prince-connor/connor-whoa.png`)
      ],
      [
        require(`@/assets/images/lance/lance-default.png`),
        require(`@/assets/images/lance/lance-default.png`),
        require(`@/assets/images/lance/lance-angry.png`),
        require(`@/assets/images/lance/lance-wild.png`),
        require(`@/assets/images/lance/lance-reading.png`)
      ],
      [
        require(`@/assets/images/milo/milo-default.png`),
        require(`@/assets/images/milo/milo-default.png`),
        require(`@/assets/images/milo/milo-troubled.png`),
        require(`@/assets/images/milo/milo-clearthroat.png`),
        require(`@/assets/images/milo/milo-nervous.png`),
        require(`@/assets/images/milo/milo-sorry.png`),
        require(`@/assets/images/milo/milo-confused.png`),
        require(`@/assets/images/milo/milo-smile.png`),
        require(`@/assets/images/milo/milo-sad.png`)
      ],
      [
        require(`@/assets/images/eric/eric-default.png`),
        require(`@/assets/images/eric/eric-default.png`),
        require(`@/assets/images/eric/eric-angry.png`)
      ],
      [
        require(`@/assets/images/harriet/harriet-default.png`),
        require(`@/assets/images/harriet/harriet-default.png`),
        require(`@/assets/images/harriet/harriet-angry.png`)
      ],
      [
        require(`@/assets/images/hugo/hugo-default.png`),
        require(`@/assets/images/hugo/hugo-default.png`),
        require(`@/assets/images/hugo/hugo-angry.png`)
      ],
      [
        require(`@/assets/images/humpty/humpty-default.png`),
        require(`@/assets/images/humpty/humpty-default.png`),
        require(`@/assets/images/humpty/humpty-angry.png`),
        require(`@/assets/images/humpty/humpty-vote.png`)
      ],
      [
        require(`@/assets/images/melanie/melanie-default.png`),
        require(`@/assets/images/melanie/melanie-default.png`)
      ],
      [
        require(`@/assets/images/richard/richard-default.png`),
        require(`@/assets/images/richard/richard-default.png`)
      ],
      [
        require(`@/assets/images/skynnard/skynnard-default.png`),
        require(`@/assets/images/skynnard/skynnard-default.png`),
        require(`@/assets/images/skynnard/skynnard-sad.png`),
        require(`@/assets/images/skynnard/skynnard-shock.png`)
      ],
      [
        require(`@/assets/images/victor/victor-default.png`),
        require(`@/assets/images/victor/victor-default.png`),
        require(`@/assets/images/victor/victor-sad.png`)
      ],
      [
        require(`@/assets/images/zane/zane-default.png`),
        require(`@/assets/images/zane/zane-default.png`),
        require(`@/assets/images/zane/zane-angry.png`),
        require(`@/assets/images/zane/zane-sad.png`)
      ],
      [
        require(`@/assets/images/vox/vox-default.png`),
        require(`@/assets/images/vox/vox-default.png`)
      ],
      [
        require(`@/assets/images/thumper/thumper-default.png`),
        require(`@/assets/images/thumper/thumper-default.png`),
        require(`@/assets/images/thumper/thumper-sad.png`),
        require(`@/assets/images/thumper/thumper-robe-default.png`),
        require(`@/assets/images/thumper/thumper-robe-sad.png`)
      ],
      [
        require(`@/assets/images/majorpocket/majorpocket-default.png`),
        require(`@/assets/images/majorpocket/majorpocket-default.png`),
        require(`@/assets/images/majorpocket/majorpocket-sleep.png`)
      ],
      [
        require(`@/assets/images/tigeress/tigeress-default.png`),
        require(`@/assets/images/tigeress/tigeress-default.png`),
        require(`@/assets/images/tigeress/tigeress-angry.png`),
        require(`@/assets/images/tigeress/tigeress-rally.png`),
        require(`@/assets/images/tigeress/tigeress-talk.png`),
        require(`@/assets/images/tigeress/tigeress-talk-angry.png`)
      ],
      [
        require(`@/assets/images/king/king-happy.png`),
        require(`@/assets/images/king/king-happy.png`),
        require(`@/assets/images/king/king-mourning.png`),
        require(`@/assets/images/king/king-stern.png`),
        require(`@/assets/images/king/king-worried.png`),
        require(`@/assets/images/king/king-armor-mourning.png`),
        require(`@/assets/images/king/king-armor-stern.png`),
        require(`@/assets/images/king/king-armor-worried.png`)
      ],
      [
        require(`@/assets/images/galen/galen-default.png`),
        require(`@/assets/images/galen/galen-default.png`)
      ],
      [
        require(`@/assets/images/bob/bob-default.png`),
        require(`@/assets/images/bob/bob-default.png`)
      ]
    ],
    questions: {
      initialQuestion: {
        routes: [
          {
            id: 1,
            route: '/story11'
          },
          {
            id: 2,
            route: '/story12'
          }
        ],
        choices: [
          {
            id: 1,
            text: 'Seek help from the A.P.A.'
          },
          {
            id: 2,
            text: 'Deal with the Tigers directly'
          }
        ],
        MCQ: [
          {
            id: 1,
            event: '“Is this really the right choice?” thought Prince Connor to himself. He tried to remember something he saw in the Apeollo Seven report...',
            feedback: 'Multilateral diplomacy requires consensus, which may be difficult to achieve or take too long!',
            question: 'Which of the following might be a disadvantage for multilateral negotiations (dealing with the tigers through the Alliance of Plains Animals)?',
            reactions: [
              {
                id: 1,
                react: '“A truly wise choice, Prince Connor,” said Milo. “Your erudition will make you a great king.”'
              },
              {
                id: 2,
                react: '“What? That is not right at all!” Lance said heatedly. “We’re not prepared to go to the A.P.A.”\n' +
                '\n' +
                'Milo sighed. “Lance has a point, my Prince.”'
              }
            ],
            choices: [
              {
                id: 1,
                option: 'A',
                text: 'Multilateral diplomacy facilitates partnerships as the tigers threaten all plains animals and are not only a problem for the apes.'
              },
              {
                id: 2,
                option: 'B',
                text: 'Multilateral diplomacy requires consensus between members of the Alliance in order to reach an agreement.',
                correct: true
              },
              {
                id: 3,
                option: 'C',
                text: 'Multilateral diplomacy allows the smaller animals to negotiate with the tigers by providing “safety in numbers”.'
              }
            ]
          },
          {
            id: 2,
            event: '“Is this really the right choice?” thought Prince Connor to himself. He tried to remember something he saw in the Apeollo Seven report...',
            feedback: 'Multilateral agreements make all signatories treat each other the same. That means no country can give better trade deals to one country than it does to another. That levels the playing field. This is not necessarily the case for bilateral negotiations where only two countries are involved at any one time, and where usually countries with more to offer are targeted.',
            question: 'Which option is <span class="underline">not</span> an advantage of bilateral negotiations (dealing with the tigers directly)?',
            reactions: [
              {
                id: 1,
                react: 'Lance punched the air in joy. “You definitely have what it takes to deal with the tigers directly, Your Majesty.”'
              },
              {
                id: 2,
                react: '“Wrong, wrong, wrong!” Milo said. “Did you learn nothing of planet Earth’s politics? We will take this to the A.P.A.!”'
              }
            ],
            choices: [
              {
                id: 1,
                option: 'A',
                text: 'Typically faster progress in negotiations as compared to multilateral negotiations.'
              },
              {
                id: 2,
                option: 'B',
                text: 'Levels the playing field.',
                correct: true
              },
              {
                id: 3,
                option: 'C',
                text: 'Higher success rate of negotiation, by avoiding the multilateral deadlock.'
              }
            ]
          }
        ]
      },
      A: {
        0: [
          {
            id: 1,
            title: '1.1: SEEK HELP FROM THE ALLIANCE OF PLAINS ANIMALS',
            routes: [
              {
                id: 1,
                route: '/story111'
              },
              {
                id: 2,
                route: '/story112'
              }
            ],
            choices: [
              {
                id: 1,
                text: 'IMPOSE SANCTIONS'
              },
              {
                id: 2,
                text: 'FORM DEFENSIVE PACT'
              }
            ],
            MCQ: [
              {
                id: 1,
                event: 'Remembering the Apeollo Seven report, Prince Connor considered the effectiveness of imposing sanctions.',
                feedback: '“Sanctions are sometimes considered a middle ground between the options of war and diplomacy.”',
                question: 'Which of the following foreign policy goals is a reason why international bodies like the United Nations apply economic sanctions?',
                reactions: [
                  {
                    id: 1,
                    react: '“You are truly knowledgeable on the matter of political sanctions, Prince Connor,” said General Humpty, impressed. “We shall make it policy, then. Impose sanctions upon the tigers!”'
                  },
                  {
                    id: 2,
                    react: '“I think you are way above your head here, Prince Connor,” said Eric the Grey. “I encourage you not to vote for matters you know nothing of. Vote for the defensive pact.”\n' +
                    '\n' +
                    'Prince Connor was embarrassed. “Let us form the defensive pact,” he said sheepishly.'
                  }
                ],
                choices: [
                  {
                    id: 1,
                    option: 'A',
                    text: 'Counterterrorism.'
                  },
                  {
                    id: 2,
                    option: 'B',
                    text: 'Democracy and human rights promotion.'
                  },
                  {
                    id: 3,
                    option: 'C',
                    text: 'Conflict resolution.'
                  },
                  {
                    id: 4,
                    option: 'D',
                    text: 'All of the above.',
                    correct: true
                  }
                ]
              },
              {
                id: 2,
                event: 'Remembering the Apeollo Seven report, Prince Connor thought of how Earth’s nations had also sometimes tried to form defensive pacts.',
                feedback: 'Collective defense may also lead to closer relationships between military allies and thus afford greater peace and stability.',
                question: 'True / False? Collective defense means that allies are obligated to provide military support in the event you are attacked.',
                reactions: [
                  {
                    id: 1,
                    react: '“A wise choice indeed, Prince Connor,” said General Humpty. “Especially given your command of defensive pacts.”'
                  },
                  {
                    id: 2,
                    react: '“With all due respect, Prince Connor,” said Princess Melanie, “Is it not wiser for you to choose something that you are more familiar with? You are clearly not well-versed in the art of defensive pacts.”'
                  }
                ],
                choices: [
                  {
                    id: 1,
                    option: 'A',
                    text: 'TRUE',
                    correct: true
                  },
                  {
                    id: 2,
                    option: 'B',
                    text: 'FALSE'
                  }
                ]
              }
            ]
          }
        ],
        1: [
          {
            id: 1,
            title: '1.1.1: IMPOSE SANCTIONS',
            routes: [
              {
                id: 1,
                route: '/story1111'
              },
              {
                id: 2,
                route: '/story1112'
              }
            ],
            choices: [
              {
                id: 1,
                text: 'LIFT THE SANCTIONS'
              },
              {
                id: 2,
                text: 'EXTEND THE SANCTIONS'
              }
            ],
            MCQ: [
              {
                id: 1,
                event: 'King Connor knew that the Alliance needed to lift the sanctions. But was it the right thing to do?',
                feedback: 'Lifting sanctions may weaken their effectiveness as a deterrent in future.',
                question: 'Which of the following reasons does not support the lifting of economic sanctions?',
                reactions: [
                  {
                    id: 1,
                    react: '“Lifting sanctions is clearly the right thing to do,” said Eric the Grey. “You know how it works out on planet Earth.”'
                  },
                  {
                    id: 2,
                    react: 'Princess Melanie spoke, her gentle voice fracturing with barely controlled anger. “You’re clearly uncertain about the benefits – if any – of lifting these sanctions.”\n' +
                    '\n' +
                    'Prince Connor nodded solemnly. “I vote for extending the sanctions.”'
                  }
                ],
                choices: [
                  {
                    id: 1,
                    option: 'A',
                    text: 'To deter the tigers from further acts of aggression in the future.',
                    correct: true
                  },
                  {
                    id: 2,
                    option: 'B',
                    text: 'To alleviate the plight of the many tigers who are suffering in impoverished conditions because of the sanctions.'
                  },
                  {
                    id: 3,
                    option: 'C',
                    text: 'To improve diplomatic ties between the tigers and the other animals, which have worsened because of resentment toward the sanctions.'
                  }
                ]
              },
              {
                id: 2,
                event: 'King Connor knew that the Alliance needed to extend the sanctions. But was it the right thing to do?',
                feedback: 'Extending sanctions is always likely to worsen diplomatic relations by causing resentment at the conditions they impose.',
                question: 'True / False? Extending the sanctions is a move that will be well-received by the tigers, improving diplomatic relations.',
                reactions: [
                  {
                    id: 1,
                    react: '“You clearly know what you’re talking about,” said General Humpty. “Let it pass into policy that we shall extend these sanctions against the tigers.”'
                  },
                  {
                    id: 2,
                    react: 'Princess Melanie spoke, her gentle voice fracturing with barely controlled anger. “You’re clearly uncertain about the benefits – if any – of lifting these sanctions.”\n' +
                    '\n' +
                    'Prince Connor nodded solemnly. “I vote for extending the sanctions.'
                  }
                ],
                choices: [
                  {
                    id: 1,
                    option: 'A',
                    text: 'True'
                  },
                  {
                    id: 2,
                    option: 'B',
                    text: 'False',
                    correct: true
                  }
                ]
              }
            ]
          },
          {
            id: 2,
            title: '1.1.2: FORM A DEFENSIVE PACT',
            routes: [
              {
                id: 1,
                route: '/story1121'
              },
              {
                id: 2,
                route: '/story1122'
              }
            ],
            choices: [
              {
                id: 1,
                text: 'DECLARE WAR'
              },
              {
                id: 2,
                text: 'SUE FOR PEACE'
              }
            ],
            MCQ: [
              {
                id: 1,
                event: 'King Connor recalled a song on planet Earth which went: “War, what is it good for? Absolutely nothing!” Of course, he was not going to base his political decisions on a song by a group called The Temptations.',
                feedback: 'While economic gain may not appear to provide a just cause for war, there are very often economic reasons underlying most conflicts.',
                question: 'Which of the following is not usually considered a just cause for war?',
                reactions: [
                  {
                    id: 1,
                    react: '“You’ve learned much from the efforts of Earth’s defensive alliances. That knowledge will serve us well in the war to come,” said Prince Jude.'
                  },
                  {
                    id: 2,
                    react: '“Let us not act brashly,” Councillor Harriet reiterated. “This is an emotional time for us, and we are making decisions based on raw feeling and,” she looked pointedly at King Connor, “a dire lack of knowledge on the matter. Let us look for peace with the tigers.”'
                  }
                ],
                choices: [
                  {
                    id: 1,
                    option: 'A',
                    text: 'Self-defence.'
                  },
                  {
                    id: 2,
                    option: 'B',
                    text: 'Assisting an ally who has been invaded.'
                  },
                  {
                    id: 3,
                    option: 'C',
                    text: 'Economic gain.',
                    correct: true
                  }
                ]
              },
              {
                id: 2,
                event: 'In the face of Princess Melanie’s death, peace seemed an impossible task. King Connor pondered what he had learned about Planet Earth, and the quest for peace between nations and tribes that had been at war for years.',
                feedback: 'Countries may sometimes decide there is a just cause for war, if it intends to prevent great suffering such as genocide.',
                question: 'True / False: War is always avoided at all costs, because wars necessarily cause more suffering than they prevent.',
                reactions: [
                  {
                    id: 1,
                    react: '“If we are to think about how we feel now, yes, war seems to be the more appealing course,” said King Connor to his colleagues. “But if we are to preserve Terrafauna’s future for all life – our own as well as that of tigers – then we need peace.” He regaled the Alliance with stories of Earth’s efforts for peace, and the good that had come out from these efforts.'
                  },
                  {
                    id: 2,
                    react: '“Wrong! Wrong! Wrong!” shouted Prince Jude. “You clearly do not have the right skills and knowledge to bring peace to Terrafauna. We will go to war. It is our only choice.”'
                  }
                ],
                choices: [
                  {
                    id: 1,
                    option: 'A',
                    text: 'True'
                  },
                  {
                    id: 2,
                    option: 'B',
                    text: 'False',
                    correct: true
                  }
                ]
              }
            ]
          }
        ]
      },
      B: {
        0: [
          {
            id: 1,
            title: '1.2: DEAL WITH THE TIGERS DIRECTLY',
            routes: [
              {
                id: 1,
                route: '/story121'
              },
              {
                id: 2,
                route: '/story122'
              }
            ],
            choices: [
              {
                id: 1,
                text: 'DIPLOMACY'
              },
              {
                id: 2,
                text: 'MILITARY ACTION'
              }
            ],
            MCQ: [
              {
                id: 1,
                event: 'Relative to the tigers, the apes were a small, less powerful group. Can Prince Connor draw similarities between his problems and those of Earth’s smaller nations?',
                feedback: 'Diplomacy may not always be less costly than war in terms of the sacrifices a nation has to make. For instance, negotiations for peace may involve the loss of land or resources, and could negatively affect public confidence in a ruler if it is felt that diplomacy is a ‘weak’ response.',
                question: 'Which of the following is <span class="underline">not</span> always a good reason for avoiding war?',
                reactions: [
                  {
                    id: 1,
                    react: 'Milo smiled. “A wise man once said, ‘Diplomacy is nothing if not wrought with wisdom and knowledge’. You clearly bring wisdom and knowledge with you, Prince Connor.”\n' +
                    '\n' +
                    'Prince Connor was curious. “I don’t think I’ve heard that quote before. Who came up with it?”\n' +
                    '\n' +
                    'Milo’s smile widened with wryness. “Me. Just now.”'
                  },
                  {
                    id: 2,
                    react: 'Lance spat, “You clearly know nothing of diplomacy.”\n' +
                    '\n' +
                    'Milo responded angrily, “Watch how you speak to your future king, Lance!”\n' +
                    '\n' +
                    'Prince Connor held up a hand. “At ease, Milo and Lance. Unfortunately, Lance has a point. And since he seems to know better than me, let’s see where Lance’s idea will take us, then.”'
                  }
                ],
                choices: [
                  {
                    id: 1,
                    option: 'A',
                    text: 'Because the aggressor is much more powerful militarily than your nation.'
                  },
                  {
                    id: 2,
                    option: 'B',
                    text: 'Because war is always costlier than diplomacy.',
                    correct: true
                  },
                  {
                    id: 3,
                    option: 'C',
                    text: 'Because the citizens are strongly against war.'
                  }
                ]
              },
              {
                id: 2,
                event: 'According to the Apeollo Seven report, Earth had gone through as many conflicts – if not more – as Terrafauna. He tried to recall the significance the military played in some of Earth’s major nations, and if he could glean an advantage from their history.',
                feedback: 'It depends on which perspective you are taking. For example, an invader may consider his war economically justified, but his neighbours may condemn the war as morally unjustified.',
                question: 'War is always justified. Is this true?',
                reactions: [
                  {
                    id: 1,
                    react: 'Lance took Prince Connor’s hand in his. “With your knowledge of the military, you can lead us into battle against these wretched tigers.”'
                  },
                  {
                    id: 2,
                    react: '“This is no trivial matter, Prince Connor,” said Milo gravely. “If you are not so certain about militarism, perhaps a more diplomatic approach might be best?”'
                  }
                ],
                choices: [
                  {
                    id: 1,
                    option: 'A',
                    text: 'True'
                  },
                  {
                    id: 2,
                    option: 'B',
                    text: 'False'
                  },
                  {
                    id: 3,
                    option: 'C',
                    text: 'It is complicated.',
                    correct: true
                  }
                ]
              }
            ]
          }
        ],
        1: [
          {
            id: 1,
            title: '1.2.1: DIPLOMACY',
            routes: [
              {
                id: 1,
                route: '/story1211'
              },
              {
                id: 2,
                route: '/story1212'
              }
            ],
            choices: [
              {
                id: 1,
                text: 'COMPROMISE AND GIVE THE TIGERS WHAT THEY WANT'
              },
              {
                id: 2,
                text: 'STAND YOUR GROUND AND TRY TO WORK OUT A BETTER DEAL'
              }
            ],
            MCQ: [
              {
                id: 1,
                event: 'While Prince Connor saw his decision as a means of postponing or even preventing violence and unnecessary death, he knew that his critics would call it appeasement.',
                feedback: 'Appeasement does not always lead to peace and mutual satisfaction, in fact it may goad the aggressor into asking for more, ultimately leading to war when the country practising appeasement is unable to keep giving up land or resources.',
                question: 'What is a definite advantage to appeasement (giving the tigers what they want)?',
                reactions: [
                  {
                    id: 1,
                    react: '“I don’t like the idea of giving the tigers what they want,” conceded Lance, “but you seem to know what you’re talking about.”'
                  },
                  {
                    id: 2,
                    react: '“You know nothing of appeasement, Prince Connor,” Lance spat. “You will lead us to our demise with your lack of knowledge.”'
                  }
                ],
                choices: [
                  {
                    id: 1,
                    option: 'A',
                    text: 'There will be peace and mutual satisfaction.'
                  },
                  {
                    id: 2,
                    option: 'B',
                    text: 'Diplomacy would have been achieved.'
                  },
                  {
                    id: 3,
                    option: 'C',
                    text: 'None of the above.',
                    correct: true
                  }
                ]
              },
              {
                id: 2,
                event: 'The tricky thing about diplomacy, especially with somebody as sinister as a knife glinting in the dark like Queen Cayenne, is that one can never know for sure if it will definitely prevent war.',
                feedback: 'Acts of diplomacy require all parties involved to come to a mutual agreement, and if this is not achieved, armed conflict may ultimately still occur.',
                question: '‘Acts of diplomacy always prevent armed conflict from occurring.’ Is this true?',
                reactions: [
                  {
                    id: 1,
                    react: '“And how exactly will we work out a better deal?” Milo asked.\n' +
                    '\n' +
                    'Prince Connor told him, using examples from planet Earth’s history to support his argument.\n' +
                    '\n' +
                    'Milo nodded as he listened to his Prince. “You have a fair point, Your Majesty,” he said eventually.'
                  },
                  {
                    id: 2,
                    react: '“And how exactly will we work out a better deal?” Milo asked.\n' +
                    '\n' +
                    'Prince Connor opened his mouth to answer, but found he had nothing to say.\n' +
                    '\n' +
                    '“We’re giving them what they want!” Milo said firmly.'
                  }
                ],
                choices: [
                  {
                    id: 1,
                    option: 'A',
                    text: 'True'
                  },
                  {
                    id: 2,
                    option: 'B',
                    text: 'False',
                    correct: true
                  }
                ]
              }
            ]
          },
          {
            id: 2,
            title: '1.2.2: WAR!',
            routes: [
              {
                id: 1,
                route: '/story1221'
              },
              {
                id: 2,
                route: '/story1222'
              }
            ],
            choices: [
              {
                id: 1,
                text: 'ADOPT GUERILLA TACTICS'
              },
              {
                id: 2,
                text: 'SEEK AN ALLIANCE WITH THE LIONS'
              }
            ],
            MCQ: [
              {
                id: 1,
                event: 'Prince Connor was on board with guerilla tactics. The thing about guerilla tactics, however, was that the line between success and failure was very thin.',
                feedback: 'Guerilla warfare often takes place amidst civilians and the militarily weaker side may end up needing to forcefully enlist civilians.',
                question: 'Which of the following are possible implications of choosing guerilla warfare?',
                reactions: [
                  {
                    id: 1,
                    react: 'Major Pockets was impressed. “Prince Connor, the breadth and depth of your knowledge fills me with confidence to have you as our commander-in-chief.”\n' +
                    '\n' +
                    'The war council dispersed, preparing their forces for guerrilla warfare.'
                  },
                  {
                    id: 2,
                    react: 'Major Pockets was not impressed. “With respect, Your Majesty, it is clear from your answer that we are not equipped with the skill and knowledge to carry out a guerrilla campaign. Perhaps we should seek external help.”'
                  }
                ],
                choices: [
                  {
                    id: 1,
                    option: 'A',
                    text: 'Becoming accused of being terrorists.'
                  },
                  {
                    id: 2,
                    option: 'B',
                    text: 'Endangerment of civilians.'
                  },
                  {
                    id: 3,
                    option: 'C',
                    text: 'All of the above.',
                    correct: true
                  }
                ]
              },
              {
                id: 2,
                event: 'Prince Connor saw the great opportunity presented if the apes should pivot towards an alliance with the lions – the other great power in Terrafauna.',
                feedback: 'The lions are unlikely to support the apes’ cause without demanding something in return. Prince Connor could be leaping out of the frying pan and into the fire!',
                question: 'What are the possible implications of seeking an alliance with the lions (to balance power)?',
                reactions: [
                  {
                    id: 1,
                    react: '“A wise choice indeed, Your Majesty,” said Milo. “Your understanding of military strategies on planet Earth can serve us very well over here. I will prepare for our journey to the lions.”'
                  },
                  {
                    id: 2,
                    react: 'Lance scoffed. “The lions will eat you up when they hear the lack of knowledge ringing hollow in your voice! Perhaps guerrilla tactics might be our best course of action?”'
                  }
                ],
                choices: [
                  {
                    id: 1,
                    option: 'A',
                    text: 'The lions demanding something in return.'
                  },
                  {
                    id: 2,
                    option: 'B',
                    text: 'The lions not showing up at the last moment.'
                  },
                  {
                    id: 3,
                    option: 'C',
                    text: 'All of the above.',
                    correct: true
                  }
                ]
              }
            ]
          }
        ]
      }
    },
    startQuiz: false,
    isActivityDone: false,
    selectedChoiceId: 0,
    questionPool: null,
    clusterIndex: null,
    isInitialQuestion: true,
    isFinalQuestion: 1,
    questionBackground: '',
    isMuted: false
  },
  mutations: {
    isLandscape (state, status) {
      state.isLandscape = status
    },
    setVersion (state, version) {
      state.version = version
    },
    toggleMute (state) {
      state.isMuted = !state.isMuted
    },
    setActivityDone (state) {
      state.isActivityDone = true
    },
    setInitialQuestionFalse (state) {
      state.isInitialQuestion = false
    },
    setQuestionBackground (state, background) {
      state.questionBackground = background
    },
    setQuestionPool (state, pool) {
      state.questionPool = pool
    },
    incrementClusterIndex (state) {
      state.clusterIndex++
    },
    setClusterIndex (state, index) {
      state.clusterIndex = index
    },
    setFinalQuestion (state, id) {
      state.isFinalQuestion = id
    },
    setSelectedChoiceId (state, id) {
      state.selectedChoiceId = id
    },
    resetGame (state) {
      state.startQuiz = false
      state.isActivityDone = false
      state.selectedChoiceId = null
      state.questionPool = null
      state.clusterIndex = null
      state.isInitialQuestion = true
      state.isFinalQuestion = 1
      state.questionBackground = ''
    }
  },
  getters: {
    initialQuestion (state) {
      return state.questions.initialQuestion
    },
    getQuestionsFromSelectedPool (state) {
      return state.questions[state.questionPool]
    }
  },
  actions: {
    setVersion ({ commit }, version) {
      commit('setVersion', version)
    }
  }
})

import Vue from 'vue'
import Router from 'vue-router'
import Copyright from '@/components/Copyright'
import Credits from '@/components/Credits'
import Introduction from '@/components/Introduction'
import Character from '@/components/Character'
import Story1 from '@/components/Story1'
import QuestionOne from '@/components/QuestionOne'
import Story11 from '@/components/Story11'
import Story12 from '@/components/Story12'
import QuestionTwo from '@/components/QuestionTwo'
import Story111 from '@/components/Story111'
import Story112 from '@/components/Story112'
import Story121 from '@/components/Story121'
import Story122 from '@/components/Story122'
import Story1111 from '@/components/Story1111'
import Story1112 from '@/components/Story1112'
import Story1121 from '@/components/Story1121'
import Story1122 from '@/components/Story1122'
import Story1211 from '@/components/Story1211'
import Story1212 from '@/components/Story1212'
import Story1221 from '@/components/Story1221'
import Story1222 from '@/components/Story1222'

Vue.use(Router)

export default new Router({
  base: window.location.pathname,
  routes: [
    {
      path: '/',
      name: 'Copyright',
      component: Copyright,
      meta: {
        title: 'Copyright'
      }
    },
    {
      path: '/credits',
      name: 'Credits',
      component: Credits,
      meta: {
        title: 'Credits'
      }
    },
    {
      path: '/introduction',
      name: 'Introduction',
      component: Introduction,
      meta: {
        title: 'Introduction'
      }
    },
    {
      path: '/character',
      name: 'Character',
      component: Character,
      meta: {
        title: 'Character'
      }
    },
    {
      path: '/story1',
      name: 'StoryOne',
      component: Story1,
      meta: {
        title: 'Story One 1'
      }
    },
    {
      path: '/questionone',
      name: 'QuestionOne',
      component: QuestionOne,
      meta: {
        title: 'Question One'
      }
    },
    {
      path: '/story11',
      name: 'Story11',
      component: Story11,
      meta: {
        title: 'Story 1.1'
      }
    },
    {
      path: '/story12',
      name: 'Story12',
      component: Story12,
      meta: {
        title: 'Story 1.2'
      }
    },
    {
      path: '/questiontwo',
      name: 'Question Two',
      component: QuestionTwo,
      meta: {
        title: 'Question Two'
      }
    },
    {
      path: '/story111',
      name: 'Story111',
      component: Story111,
      meta: {
        title: 'Story 1.1.1'
      }
    },
    {
      path: '/story112',
      name: 'Story112.vue',
      component: Story112,
      meta: {
        title: 'Story 1.1.2'
      }
    },
    {
      path: '/story121',
      name: 'Story121',
      component: Story121,
      meta: {
        title: 'Story 1.2.1'
      }
    },
    {
      path: '/story122',
      name: 'Story122',
      component: Story122,
      meta: {
        title: 'Story 1.2.2'
      }
    },
    {
      path: '/story1111',
      name: 'Story 1.1.1.1',
      component: Story1111,
      meta: {
        title: 'Story 1 1 1 1'
      }
    },
    {
      path: '/story1112',
      name: 'Story 1.1.1.2',
      component: Story1112,
      meta: {
        title: 'Story 1.1.1.2'
      }
    },
    {
      path: '/story1121',
      name: 'Story 1 1 2 1',
      component: Story1121,
      meta: {
        title: 'Story 1.1.2.1'
      }
    },
    {
      path: '/story1122',
      name: 'Story 1 1 2 2',
      component: Story1122,
      meta: {
        title: 'Story 1.1.2.2'
      }
    },
    {
      path: '/story1211',
      name: 'Story 1.2.1.1',
      component: Story1211,
      meta: {
        title: 'Story 1 2 1 1'
      }
    },
    {
      path: '/story1212',
      name: 'Story 1.2.1.2',
      component: Story1212,
      meta: {
        title: 'Story 1.2.1.2'
      }
    },
    {
      path: '/story1221',
      name: 'Story 1.2.2.1',
      component: Story1221,
      meta: {
        title: 'Story 1 2 2 1'
      }
    },
    {
      path: '/story1222',
      name: 'Story 1.2.2.2',
      component: Story1222,
      meta: {
        title: 'Story 1.2.2.2'
      }
    }
  ]
})

# Changelog

## v1.0.2
- Modified routes to use ESM imports

## v1.0.1
- Removed unnecessary usage of `async/await`
- Removed e2e runner stuff
- Modified config to allow site to open locally via `index.html`

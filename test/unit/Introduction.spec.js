import { shallow, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Introduction from '@/components/Introduction'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Introduction.vue', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(Introduction)

    window.HTMLMediaElement.prototype.load = () => {}
    window.HTMLMediaElement.prototype.play = () => {}
  })

  test('should increment storyCounter correctly', () => {
    expect(wrapper.vm.storyCounter).toBe(0)

    wrapper.find('.start-button').trigger('click')

    expect(wrapper.vm.storyCounter).toBe(1)
  })

  test('should navigate to characters page correctly', () => {
    const $route = {
      path: '/character'
    }

    const router = new VueRouter()

    wrapper = shallow(Introduction, {
      router, localVue
    })

    wrapper.setData({ storyCounter: 1 })

    wrapper.find('.start-button').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})

import { shallow, createLocalVue } from '@vue/test-utils'
import sinon from 'sinon'
import VueRouter from 'vue-router'
import Character from '@/components/Character'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Character.vue', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(Character)

    window.HTMLMediaElement.prototype.load = () => {}
    window.HTMLMediaElement.prototype.play = () => {}
  })

  test('should play sound effect when mouse over any buttons', () => {
    const playSoundEffectStub = sinon.stub()
    wrapper.setMethods({ playSoundEffect: playSoundEffectStub })

    wrapper.findAll('.character-front').at(0).trigger('mouseover')

    expect(playSoundEffectStub.called).toBe(true)
  })

  test('should navigate to first story correctly', () => {
    const $route = {
      path: '/story1'
    }

    const router = new VueRouter()

    wrapper = shallow(Character, {
      router, localVue
    })

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
